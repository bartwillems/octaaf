package duckduckgo

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
)

// BaseURL is the root of the ddg api url dink
const BaseURL = "https://duckduckgo.com"

// ImageResponse is the json response for a duckduckgo image query
type ImageResponse struct {
	Query string `json:"query"`
	List  []struct {
		Width  int    `json:"width"`
		Height int    `json:"height"`
		URL    string `json:"image"`
		Source string `json:"source"`
		Title  string `json:"title"`
	} `json:"results"`
}

// GetImages fetches an image list from duckduckgo
func GetImages(imageQuery string) (ImageResponse, error) {
	url, err := getURL(imageQuery)
	if err != nil {
		return ImageResponse{}, err
	}

	res, err := http.Get(url)

	if err != nil {
		return ImageResponse{}, err
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return ImageResponse{}, readErr
	}

	defer res.Body.Close()

	images := ImageResponse{}
	if jsonErr := json.Unmarshal(body, &images); jsonErr != nil {
		return ImageResponse{}, jsonErr
	}

	return images, nil
}

func getToken(imageQuery string) (string, error) {
	url, err := url.Parse(BaseURL)

	if err != nil {
		return "", err
	}

	query := url.Query()
	query.Set("q", imageQuery)

	url.RawQuery = query.Encode()

	res, err := http.Get(url.String())

	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	defer res.Body.Close()

	re := regexp.MustCompile(`vqd=([\d-]+)`)

	token := string(re.Find(body))

	if token == "" {
		return "", errors.New("token not found")
	}

	tokenPair := strings.Split(token, "=")

	if len(tokenPair) < 2 {
		return "", fmt.Errorf("invalid token received: %v", token)
	}
	return tokenPair[1], nil
}

func getURL(imageQuery string) (string, error) {
	url, err := url.Parse(BaseURL + "/i.js?l=us-en&o=json")
	if err != nil {
		return "", err
	}

	token, err := getToken(imageQuery)
	if err != nil {
		return "", err
	}

	query := url.Query()
	query.Set("vqd", token)
	query.Set("q", imageQuery)

	if shouldShowNSFW() {
		query.Set("p", "-1")
	}

	url.RawQuery = query.Encode()

	return url.String(), nil
}

func shouldShowNSFW() bool {
	now := time.Now()

	if now.Weekday() == time.Saturday || now.Weekday() == time.Sunday {
		return true
	}

	if now.Hour() <= 9 || now.Hour() >= 17 {
		return true
	}

	return false
}
